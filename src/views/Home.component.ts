import { Component, Vue } from 'vue-property-decorator'
import grapesJS from 'grapesjs'
import Blocks from '../config/blocks'
import { mixins } from 'vue-class-component'
import Components from '../config/components'
import Commands from '../config/commands'
import Events from '../config/events'
import { EditorConfig } from '../config/editorConfig'
const headerBlock = require('../assets/img/header_block.png')
@Component
export default class HomeComponent extends mixins(Vue, Blocks, Components, Commands, Events) {
    public editor: any
    public selectedImage: any

    constructor () {
      super()
      this.editor = null
      this.selectedImage = null
    }

    public changeSocials (e: any) {
      this.changeSocialBlock(e, this.editor)
    }

    public mounted () {
      this.editor = grapesJS.init(EditorConfig)
      const cmdm: any = this.editor.Commands
      const bm: any = this.editor.BlockManager
      const pn: any = this.editor.Panels
      this.addCommands(cmdm)
      this.removeNotNeededBlocks(bm)
      this.addBlocks(bm)
      this.addCustomComponents(this.editor)
      this.addEvents(this.editor)
    }
}
