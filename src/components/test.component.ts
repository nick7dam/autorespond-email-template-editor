import { Component, Vue } from 'vue-property-decorator'
import { mixins } from 'vue-class-component'
@Component
export default class TestComponent extends mixins(Vue) {
  constructor () {
    super()
  }
}
