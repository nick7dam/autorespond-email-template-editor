import HomeComponent from './views/Home.vue'
import 'grapesjs/dist/css/grapes.min.css'
const ComponentLibrary = {
  install (Vue, options = {}) {
    const component = HomeComponent
    Vue.component('emailTemplateEditor', component)
  }
}

export default ComponentLibrary

if (typeof window !== 'undefined' && window.Vue) {
  window.Vue.use(ComponentLibrary)
}
