import { Component, Vue } from 'vue-property-decorator'

const headerBlock = require('../assets/img/header_block.png')
const textBlock = require('../assets/img/text_block.png')
const socialBlock = require('../assets/img/social_block.png')
const singleArticleBlock = require('../assets/img/single_attribute_block.png')
const sideArticleBlock = require('../assets/img/side_article_block.png')
const tripleArticleBlock = require('../assets/img/tripleArticleBlock.png')
const titleBlock = require('../assets/img/title_block.png')
const textImageBlock = require('../assets/img/textImageBlock.png')
const doubleArticleBlock = require('../assets/img/double_article_block.png')
const buttonBlock = require('../assets/img/button_block.png')
const codeBlock = '' // require('../assets/img/code_block.png')
@Component
export default class Blocks extends Vue {
    public selectedImage: any
    public socialOptions: any
    public globalStyle: any
    constructor () {
      super()
      this.selectedImage = null
      this.globalStyle = {
        headers: {
          color: '#ffffff',
          fontSize: '12',
          fontFamily: 'Roboto',
          fontStyle: 'normal',
          fontWeight: '100'
        },
        buttons: {
          color: '#ffffff',
          backgroundColor: '#ffffff',
          fontSize: '12',
          fontFamily: 'Roboto',
          fontStyle: 'normal',
          fontWeight: '100'
        },
        texts: {
          color: '#ffffff',
          fontSize: '12',
          fontFamily: 'Roboto',
          fontStyle: 'normal',
          fontWeight: '100'
        },
        containers: {
          body: {
            backgroundColor: '#ffffff'
          },
          containers: {
            backgroundColor: '#ffffff'
          }
        }
      }
      this.socialOptions = `<mj-section class="defaultPadding" style-type="containers" padding="0" data-gjs-type="mj-group-social">
<mj-column><mj-social font-size="12px" icon-size="24px" border-radius="12px" mode="horizontal">
        <mj-social-element name="facebook"></mj-social-element>
        <mj-social-element name="instagram"></mj-social-element>
        <mj-social-element name="twitter"></mj-social-element>
        <mj-social-element name="linkedin"></mj-social-element>
      </mj-social></mj-column></mj-section>`
    }

    public changeSocialBlock (p: any, editor: any) {
      if (p && p.attributes && p.attributes.attributes && p.attributes.attributes.name !== 'Social') return
      let socialElements: any = ''
      for (const key in p.attributes.attributes) {
        if (p.attributes.attributes.hasOwnProperty(key) && p.attributes.attributes[key] === true) {
          socialElements += `<mj-social-element name="${key}"></mj-social-element>`
        }
      }
      this.socialOptions = `<mj-section style-type="containers" class="defaultPadding" padding="0" data-gjs-type="mj-group-social">
<mj-column><mj-social font-size="12px" icon-size="24px" border-radius="12px" mode="horizontal">
        ${socialElements}
      </mj-social></mj-column></mj-section>`
      const sel = editor.getSelected()
      sel.components(this.socialOptions)
    }

    public addBlocks (bm: any) {
      const self = this
      bm.add('headerBlock', {
        label: `<b>Header Block</b><br/><img class="block-title" width="100%" src="${headerBlock}"/>`,
        attributes: { class: 'gjs-block-section' },
        content: `<mj-section class="defaultPadding" data-block-id="headerBlock" data-gjs-type="default">
    <mj-column style-type="containers" class="image_container">
      <mj-image src="${this.selectedImage ? this.selectedImage : 'http://placehold.it/350x250/78c5d6/fff'}"/>
    </mj-column>
  </mj-section>`,
        type: 'image'
      })
      bm.add('textBlock', {
        label: `<b>Text Block</b><br/><img class="block-title" width="100%" src="${textBlock}"/>`,
        type: 'text',
        content: `<mj-section data-gjs-type="default" class="defaultPadding">
    <mj-column style-type="containers" data-gjs-type="mj-column">
      <mj-text style-type="texts" style-type="texts" data-block-id="textBlock">Insert text here</mj-text>
    </mj-column>
  </mj-section>`
      })
      bm.add('social', {
        label: `<b>Social Block</b><br/><img class="block-title" width="100%" src="${socialBlock}"/>`,
        select: true,
        content: self.socialOptions,
        activate: true
      })
      bm.add('singleArticleBlock', {
        label: `<b>Single Article Block</b><br/><img class="block-title" width="100%" src="${singleArticleBlock}"/>`,
        select: true,
        content: `<mj-section data-gjs-type="default" class="defaultPadding">
    <mj-column style-type="containers">
      <mj-image src="${this.selectedImage ? this.selectedImage : 'http://placehold.it/350x250/78c5d6/fff'}"></mj-image>
      <mj-section data-gjs-type="text">
        <mj-column data-gjs-type="mj-column">
          <mj-text style-type="headers" style-type="headers" data-block-id="singleArticleBlockTitle" font-size="18px">Title block goes here</mj-text>
        </mj-column>
      </mj-section>
      <mj-section data-gjs-type="text">
        <mj-column>
          <mj-text style-type="texts" style-type="texts" data-block-id="singleArticleBlockText">Content 1</mj-text>
        </mj-column>
      </mj-section>
      <mj-section data-gjs-type="default">
        <mj-column>
          <mj-button style-type="buttons" style-type="buttons" data-block-id="singleArticleBlockButton">Button</mj-button>
        </mj-column>
      </mj-section>
    </mj-column>
  </mj-section>`,
        activate: true
      })
      bm.add('sideArticleBlock', {
        label: `<b>Side Article Block</b><br/><img class="block-title" width="100%" src="${sideArticleBlock}"/>`,
        select: true,
        content: `<mj-section data-gjs-type="default" data-block-id="sideArticleBlock">
<mj-column  data-block-id="singleArticleBlockColumn" style-type="containers">
    <mj-column>
      <mj-image src="${this.selectedImage ? this.selectedImage : 'http://placehold.it/350x250/78c5d6/fff'}"></mj-image>
    </mj-column>
        <mj-column  height="300px">
         <mj-text style-type="headers" data-block-id="singleArticleBlockTitle">Title block goes here</mj-text>
         <mj-text style-type="texts" data-block-id="singleArticleBlockText">Text goes here</mj-text>
        </mj-column>
  </mj-section><mj-section style-type="containers" data-gjs-type="default"><mj-column style-type="containers"><mj-button style-type="buttons" data-block-id="singleArticleBlockButton">Button</mj-button></mj-column></mj-section>`,
        activate: true
      })
      bm.add('tripleArticleBlock', {
        label: `<b>Triple Article Block</b><br/><img class="block-title" width="100%" src="${tripleArticleBlock}"/>`,
        select: true,
        content: `<mj-section data-gjs-type="table" width="100%" data-block-id="tripleArticleBlock">
    <mj-column style-type="containers" data-gjs-type="mj-column" data-block-id="singleArticleBlockColumn">
      <mj-image src="${this.selectedImage ? this.selectedImage : 'http://placehold.it/350x250/78c5d6/fff'}"></mj-image>
      <mj-section data-gjs-type="text">
        <mj-column>
          <mj-text style-type="headers" data-block-id="singleArticleBlockTitle">Title block goes here</mj-text>
        </mj-column>
      </mj-section>
      <mj-section data-gjs-type="text">
        <mj-column>
          <mj-text style-type="texts" data-block-id="singleArticleBlockText">Content 1</mj-text>
        </mj-column>
      </mj-section>
      <mj-section data-gjs-type="default">
        <mj-column>
          <mj-button style-type="buttons" data-block-id="singleArticleBlockButton">Button</mj-button>
        </mj-column>
      </mj-section>
    </mj-column>
    <mj-column style-type="containers"  data-gjs-type="mj-column" data-block-id="singleArticleBlockColumn">
      <mj-image src="${this.selectedImage ? this.selectedImage : 'http://placehold.it/350x250/78c5d6/fff'}"></mj-image>
      <mj-section data-gjs-type="text">
        <mj-column>
          <mj-text style-type="headers" data-block-id="singleArticleBlockTitle">Title block goes here</mj-text>
        </mj-column>
      </mj-section>
      <mj-section data-gjs-type="text">
        <mj-column>
          <mj-text style-type="texts" data-block-id="singleArticleBlockText">Content 2</mj-text>
        </mj-column>
      </mj-section>
      <mj-section data-gjs-type="default">
        <mj-column>
          <mj-button style-type="buttons" data-block-id="singleArticleBlockButton">Button</mj-button>
        </mj-column>
      </mj-section>
    </mj-column>
    <mj-column style-type="containers" data-gjs-type="mj-column" data-block-id="singleArticleBlockColumn">
      <mj-image src="${this.selectedImage ? this.selectedImage : 'http://placehold.it/350x250/78c5d6/fff'}"></mj-image>
      <mj-section data-gjs-type="text">
        <mj-column>
          <mj-text style-type="headers" data-block-id="singleArticleBlockTitle">Title block goes here</mj-text>
        </mj-column>
      </mj-section>
      <mj-section data-gjs-type="text">
        <mj-column>
          <mj-text style-type="texts" data-block-id="singleArticleBlockText">Content 3</mj-text>
        </mj-column>
      </mj-section>
      <mj-section data-gjs-type="default">
        <mj-column>
          <mj-button style-type="buttons" data-block-id="singleArticleBlockButton">Button</mj-button>
        </mj-column>
      </mj-section>
    </mj-column>
  </mj-section>`,
        activate: true
      })
      bm.add('titleBlock', {
        label: `<b>Title Block</b><br/><img class="block-title" width="100%" src="${titleBlock}"/>`,
        select: true,
        content: `<mj-section data-gjs-type="table" class="button">
    <mj-column style-type="containers" data-gjs-type="row" class="button">
          <mj-text style-type="headers" data-block-id="titleBlock">Title block goes here</mj-text>
</mj-column>
</mj-section>`,
        activate: true
      })
      bm.add('textImageBlock', {
        label: `<b>Text Image Block</b><br/><img class="block-title" width="100%" src="${textImageBlock}"/>`,
        select: true,
        content: `
      <mj-section data-gjs-type="default" data-block-id="textImageBlock">
        <mj-column style-type="containers">
          <mj-image src="${this.selectedImage ? this.selectedImage : 'http://placehold.it/350x250/78c5d6/fff'}"></mj-image>
        </mj-column>
      </mj-section>
      <mj-section data-gjs-type="text">
        <mj-column style-type="containers">
          <mj-text style-type="texts">Your content goes here</mj-text>
        </mj-column>
      </mj-section>`,
        activate: true
      })
      bm.add('doubleArticleBlock', {
        label: `<b>Double Article Block</b><br/><img class="block-title" width="100%" src="${doubleArticleBlock}"/>`,
        select: true,
        content: `<mj-section data-gjs-type="default" data-block-id="doubleArticleBlock">
    <mj-column style-type="containers" data-block-id="singleArticleBlockColumn">
      <mj-image src="${this.selectedImage ? this.selectedImage : 'http://placehold.it/350x250/78c5d6/fff'}"></mj-image>
      <mj-section data-gjs-type="title">
        <mj-column>
          <mj-text style-type="headers" data-block-id="singleArticleBlockTitle">Title block goes here</mj-text>
        </mj-column>
      </mj-section>
      <mj-section data-gjs-type="text">
        <mj-column>
          <mj-text style-type="texts" data-block-id="singleArticleBlockText">Content 1</mj-text>
        </mj-column>
      </mj-section>
      <mj-section data-gjs-type="default">
        <mj-column>
          <mj-button style-type="buttons" data-block-id="singleArticleBlockButton">Button</mj-button>
        </mj-column>
      </mj-section>
    </mj-column>
    <mj-column style-type="containers" data-block-id="singleArticleBlockColumn">
      <mj-image src="${this.selectedImage ? this.selectedImage : 'http://placehold.it/350x250/78c5d6/fff'}"></mj-image>
      <mj-section data-gjs-type="title">
        <mj-column>
          <mj-text style-type="headers" data-block-id="singleArticleBlockTitle">Title block goes here</mj-text>
        </mj-column>
      </mj-section>
      <mj-section data-gjs-type="text">
        <mj-column>
          <mj-text style-type="texts" data-block-id="singleArticleBlockText">Content 1</mj-text>
        </mj-column>
      </mj-section>
      <mj-section data-gjs-type="default">
        <mj-column>
          <mj-button style-type="buttons" data-block-id="singleArticleBlockButton">Button</mj-button>
        </mj-column>
      </mj-section>
    </mj-column>
  </mj-section>`,
        activate: true
      })
      bm.add('buttonBlock', {
        label: `<b>Button Block</b><br/><img class="block-title" width="100%" src="${buttonBlock}"/>`,
        select: true,
        content: ` 
      <mj-section data-gjs-type="link" class="button">
        <mj-column style-type="containers" data-block-id="singleArticleBlockColumn" class="button">
          <mj-button style-type="buttons" data-block-id="singleArticleBlockButton">Button</mj-button>
        </mj-column>
      </mj-section>`,
        activate: false
      })
      bm.add('custom-code', {
        label: `<b>Code Block</b><br/><img class="block-title" width="100%" src="${codeBlock}"/>`,
        attributes: { class: 'gjs-block-section' },
        content: '<mj-section data-gjs-type="default" data-block-id="customCode"><div data-gjs-type="custom-code"></div></mj-section>'
      })
    }

    public removeNotNeededBlocks (bm: any) {
      bm.remove('mj-1-column')
      bm.remove('mj-2-columns')
      bm.remove('mj-3-columns')
      bm.remove('mj-text')
      bm.remove('mj-button')
      bm.remove('mj-spacer')
      bm.remove('mj-image')
      bm.remove('mj-divider')
      bm.remove('mj-wrapper')
      bm.remove('mj-hero')
      bm.remove('mj-navbar')
      bm.remove('mj-navbar-link')
      bm.remove('mj-social-element')
      bm.remove('mj-social-group')
      bm.remove('custom-code')
    }
}
