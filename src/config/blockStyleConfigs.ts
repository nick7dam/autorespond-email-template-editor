import { Fonts, MjmlFonts } from './fonts'

export const bgColor = {
  type: 'color',
  name: 'Background Color', // Label for the property
  property: 'background-color', // CSS property (if buildProps contains it will be extended)
  defaults: '#FFFFFF' // Default value
}
export const bg = {
  name: 'Container background color',
  property: 'container-background-color',
  type: 'color'
}
export const fontFamily = {
  type: 'select',
  name: 'Font Family',
  property: 'font-family',
  defaults: Fonts[1].value,
  list: Fonts
}
export const color = {
  type: 'color',
  name: 'Color',
  property: 'color',
  defaults: '#FFFFFF'
}
export const fontSize = {
  type: 'integer',
  units: ['px', '%'],
  name: 'Font Size',
  property: 'font-size',
  defaults: '12px'
}
export const fontWeight = {
  type: 'select',
  name: 'Font Weight',
  property: 'font-weight',
  defaults: '12px',
  list: [{
    name: '100',
    value: '100'
  }, {
    name: '200',
    value: '200'
  }, {
    name: '400',
    value: '400'
  }, {
    name: 'Bold',
    value: 'bold'
  }]
}

export const fontStyle = {
  type: 'select',
  name: 'Font Style',
  property: 'font-style',
  defaults: '12px',
  list: [{
    name: 'Normal',
    value: 'normal'
  }, {
    name: 'Italic',
    value: 'italic'
  }]
}
export const StyleConfig = {
  headerBlock: {
    style: [bg],
    buildProps: ['container-background-color']
  },
  textBlock: {
    style: [fontFamily, fontSize, fontWeight, color, fontStyle, bg],
    buildProps: ['font-family', 'font-size', 'font-weight', 'color', 'font-style', 'container-background-color']
  },
  textBlockColumn: {
    style: [bg],
    buildProps: ['container-background-color']
  },
  socialBlock: { style: [bg], buildProps: ['container-background-color'] },
  singleArticleBlockTitle: {
    style: [fontFamily, fontSize, fontWeight, color, fontStyle, bg],
    buildProps: ['font-family', 'font-size', 'font-weight', 'color', 'font-style', 'container-background-color']
  },
  singleArticleBlockText: {
    style: [fontFamily, fontSize, fontWeight, color, fontStyle, bg],
    buildProps: ['font-family', 'font-size', 'font-weight', 'color', 'font-style', 'container-background-color']
  },
  singleArticleBlockButton: {
    style: [fontFamily, fontSize, fontWeight, color, fontStyle, bgColor, bg],
    buildProps: ['font-family', 'font-size', 'font-weight', 'color', 'font-style', 'background-color', 'container-background-color']
  },
  titleBlock: {
    style: [fontFamily, fontSize, fontWeight, color, fontStyle, bg],
    buildProps: ['font-family', 'font-size', 'font-weight', 'color', 'font-style', 'container-background-color']
  },
  textImageBlock: {
    style: [fontFamily, fontSize, fontWeight, color, fontStyle, bg],
    buildProps: ['font-family', 'font-size', 'font-weight', 'color', 'font-style', 'container-background-color']
  },
  customCode: { style: [bg], buildProps: ['container-background-color'] }
}
