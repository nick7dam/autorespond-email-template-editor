import { Component, Vue } from 'vue-property-decorator'

@Component
export default class Commands extends Vue {
    public selectedImage: any
    public editPanel: any
    public containersProps = `<div class="row">
  <div class="col-md-12 text-white">
  <label for="containerBg">Containers Background Color</label>
<div class="gjs-field gjs-field-color">
<span class="gjs-input-holder"><input id="containerBg" name="containerBg" class="containerBg" type="color" height="100%" width="100%"/></span>
</div>
</div>
  <div class="col-md-12 text-white mt-3">
  <label for="sideContainerBg">Body Background Color</label>
<div class="gjs-field gjs-field-color">
<span class="gjs-input-holder"><input type="color" id="sideContainerBg" class="sideContainerBg" name="sideContainerBg" value="#DDDDCC" height="100%" width="100%"/></span>
</div>
</div>
  </div>`
    public styleProperties = `<div class="row">
  <div class="col-md-12 text-white">
  <label>Font-Family</label>
  <select class="gjs-field pt-2 pb-2 gjs-select text-white fontFamily">
  <option value="Arial">Arial</option>
  <option value="Roboto">Roboto</option>
</select>
</div>
</div>
<div class="row">
<div class="col-md-6 pt-2 pb-2 text-white mt-2 mb-2">
<label for="fontSize">Font Size</label>
<div class="gjs-field gjs-field-integer">
<span class="gjs-input-holder"><input id="fontSize" class="fontSize" name="fontSize" type="number" min="0" placeholder="12" height="100%" width="100%"/></span>
</div>
</div>
<div class="col-md-6 pt-2 pb-2 text-white mt-2 mb-2">
<label for="fontColor">Color</label>
<div class="gjs-field gjs-field-color">
<span class="gjs-input-holder"><input id="fontColor" class="fontColor" name="fontColor" type="color" height="100%" width="100%"/></span>
</div>
</div>
</div>
<div class="row">
<div class="col-md-6">
<label>Font Weight</label>
<select class="gjs-field pt-2 pb-2 gjs-select text-white fontWeight">
  <option value="100">100</option>
  <option value="200">200</option>
  <option value="400">400</option>
  <option value="bold">Bold</option>
</select>
</div>
<div class="col-md-6">
<label>Font Style</label>
<select class="gjs-field pt-2 pb-2 gjs-select text-white fontStyle">
  <option value="normal">Normal</option>
  <option value="italic">Italic</option>
</select>
</div>
</div>`
    public clickedPanel: string

    constructor () {
      super()
      this.selectedImage = null
      this.editPanel = null
      this.clickedPanel = ''
    }

    public changePanel (panel: any) {
      this.clickedPanel = panel
    }

    public addCommands (cmdm: any) {
      const self = this
      cmdm.add('set-device-desktop', {
        run (editor) {
          editor.setDevice('Desktop')
        }
      })
      cmdm.add('set-device-tablet', {
        run (editor) {
          editor.setDevice('Tablet')
        }
      })
      cmdm.add('open-gsm', {
        run: function (editor) {
          if (self.editPanel === null) {
            const editMenuDiv = document.createElement('div')
            editMenuDiv.innerHTML = `
  <div class="gjs-sm-sector gjs-sm-sector__Styles no-select gjs-sm-open">
  <div class="gjs-sm-title collapsed" data-toggle="collapse" 
  data-target="#collapse1" aria-expanded="false" aria-controls="collapse1" data-sector-title="headers">
    Headers Style
  </div><div id="collapse1" class="gjs-sm-properties collapse">${self.styleProperties}</div></div>
  
  <div class="gjs-sm-sector gjs-sm-sector__Styles no-select gjs-sm-open">
  <div class="gjs-sm-title collapsed" data-sector-title="texts" data-toggle="collapse" 
  data-target="#collapse2" aria-expanded="false" aria-controls="collapse2" >
    Texts Style
  </div><div class="gjs-sm-properties collapse" id="collapse2">
  ${self.styleProperties}
</div></div>
  
  <div class="gjs-sm-sector gjs-sm-sector__Styles no-select gjs-sm-open gjs-sm-open">
  <div class="gjs-sm-title collapsed" data-sector-title="buttons" data-toggle="collapse" 
  data-target="#collapse3" aria-expanded="false" aria-controls="collapse3">
    Buttons Style
  </div><div class="gjs-sm-properties collapse" id="collapse3">
  ${self.styleProperties}
  <div class="row">
<div class="col-md-12 pt-2 pb-2 text-white mt-2 mb-2">
<label for="backgroundColor">Background Color</label>
<div class="gjs-field gjs-field-color">
<span class="gjs-input-holder"><input id="backgroundColor" class="backgroundColor" value="#ffffff" name="backgroundColor" type="color" height="100%" width="100%"/></span>
</div>
</div>
</div>
</div></div>
<div class="gjs-sm-sector gjs-sm-sector__Styles no-select gjs-sm-open">
  <div class="gjs-sm-title collapsed" data-toggle="collapse" 
  data-target="#collapse4" aria-expanded="false" aria-controls="collapse4" data-sector-title="containers">
    Containers Style
  </div><div id="collapse4" class="gjs-sm-properties collapse">${self.containersProps}</div></div>`
            const panels = editor.Panels.getPanel('views-container')
            panels.set('appendContent', editMenuDiv).trigger('change:appendContent')
            self.editPanel = editMenuDiv
          }
          self.editPanel.style.display = 'block'
          self.addEventListeners(editor)
        },
        stop: function (editor) {
          if (self.editPanel !== null) {
            self.editPanel.style.display = 'none'
          }
        }
      })

      cmdm.add('set-device-mobile', {
        run (editor) {
          editor.setDevice('Mobile portrait')
        }
      })
      cmdm.add('core:component-select', {
        run (editor, sender) {
        }
      })
    }

    public async iterateComponent (models, section, components?) {
      const self = this
      if (!components) {
        components = []
      }
      let i = 0
      const len: any = models.length
      while (i < len) {
        if (models[i].attributes.attributes && models[i].attributes.attributes['style-type'] && models[i].attributes.attributes['style-type'] === section) {
          components.push(models[i])
        }
        if (models[i].attributes.components && models[i].attributes.components.models && models[i].attributes.components.models.length) {
          self.iterateComponent(models[i].attributes.components.models, section, components)
        }
        i++
      }
      return components
    }

    public async applyStyle (section: string, value: any, property: string, editor: any) {
      if (section === 'body') {
        const domComponents = editor.DomComponents
        const wrapper = domComponents.getWrapper()
        wrapper.set('style', { 'padding-left': '5%', 'padding-right': '5%', 'background-color': value })
      }
      let componentModel: any = []
      componentModel = await this.iterateComponent(editor.getComponents().models, section)
      let i = 0
      const len: any = componentModel.length
      while (i < len) {
        const attributes = componentModel[i].getAttributes()
        componentModel[i].setAttributes({ ...attributes, [property]: property === 'font-size' ? value + 'px' : value })
        i++
      }
    }

    public addEventListeners (editor: any) {
      const self = this
      // change all containers background color
      $('.containerBg').on('input', function (event: any) {
        const section = $($(event.currentTarget).closest('div:has(*[data-sector-title])')[0])
          .find('.gjs-sm-title').attr('data-sector-title')
        const value = event.currentTarget.value
        // @ts-ignore
        self.applyStyle(section, value, 'background-color', editor)
      })
      // change body background color
      $('.sideContainerBg').on('input', function (event: any) {
        const section = $($(event.currentTarget).closest('div:has(*[data-sector-title])')[0])
          .find('.gjs-sm-title').attr('data-sector-title')
        const value = event.currentTarget.value
        // @ts-ignore
        self.applyStyle('body', value, 'background-color', editor)
      })
      // change font family
      $('.fontFamily').change('input', function (event: any) {
        const section = $($(event.currentTarget).closest('div:has(*[data-sector-title])')[0])
          .find('.gjs-sm-title').attr('data-sector-title')
        const value = event.currentTarget.value
        // @ts-ignore
        self.applyStyle(section, value, 'font-family')
      })
      // change font Size
      $('.fontSize').on('input', function (event: any) {
        const section = $($(event.currentTarget).closest('div:has(*[data-sector-title])')[0])
          .find('.gjs-sm-title').attr('data-sector-title')
        const value = event.currentTarget.value
        // @ts-ignore
        self.applyStyle(section, value, 'font-size', editor)
      })
      // change font Color
      $('.fontColor').on('input', function (event: any) {
        const section = $($(event.currentTarget).closest('div:has(*[data-sector-title])')[0])
          .find('.gjs-sm-title').attr('data-sector-title')
        const value = event.currentTarget.value
        setTimeout(function () {
          // @ts-ignore
          self.applyStyle(section, value, 'color', editor)
        }, 500)
      })
      // change font Weight
      $('.fontWeight').on('input', function (event: any) {
        const section = $($(event.currentTarget).closest('div:has(*[data-sector-title])')[0])
          .find('.gjs-sm-title').attr('data-sector-title')
        const value = event.currentTarget.value
        // @ts-ignore
        self.applyStyle(section, value, 'font-weight', editor)
      })
      // change font Style
      $('.fontStyle').on('input', function (event: any) {
        const section = $($(event.currentTarget).closest('div:has(*[data-sector-title])')[0])
          .find('.gjs-sm-title').attr('data-sector-title')
        const value = event.currentTarget.value
        // @ts-ignore
        self.applyStyle(section, value, 'font-style', editor)
      })
      // change font Color
      $('.backgroundColor').on('input', function (event: any) {
        const section = $($(event.currentTarget).closest('div:has(*[data-sector-title])')[0])
          .find('.gjs-sm-title').attr('data-sector-title')
        const value = event.currentTarget.value
        setTimeout(function () {
          // @ts-ignore
          self.applyStyle(section, value, 'background-color', editor)
        }, 500)
      })
    }
}
