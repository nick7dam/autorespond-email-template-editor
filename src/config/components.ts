import { Component, Vue } from 'vue-property-decorator'
@Component
export default class Components extends Vue {
  public selectedImage: any
  constructor () {
    super()
    this.selectedImage = null
  }

  public addCustomComponents (editor: any) {
    editor.DomComponents.addType('mj-group-social', {
      isComponent: el => el.tagName === 'div',
      model: {
        defaults: {
          tagName: 'mj-group-social',
          draggable: true,
          droppable: true,
          attributes: {
            type: 'div',
            name: 'Social'
          },
          traits: [
            'name',
            'placeholder',
            'mj-group-social'
          ]
        }
      }
    })
  }
}
