import { Component, Vue } from 'vue-property-decorator'
import { StyleConfig } from './blockStyleConfigs'
import { Fonts } from './fonts'
@Component
export default class Events extends Vue {
  public selectedElementStyle: any
  public selectedElementStyleBuildProps: any
  public selectedImage: any
  public styleConfig: any
  constructor () {
    super()
    this.selectedImage = null
    this.styleConfig = StyleConfig
    this.selectedElementStyleBuildProps = []
    this.selectedElementStyle = []
  }

  public addEvents (editor: any) {
    const self = this
    editor.DomComponents.addType('mj-group-social', {
      model: {
        init () {
          this.on('change:attributes', this.handleTypeChange)
        },
        handleTypeChange () {
          self.changeSocials(this)
        },
        defaults: {
          traits: [
            {
              type: 'checkbox',
              name: 'facebook',
              label: 'Facebook',
              default: true
            },
            {
              type: 'checkbox',
              name: 'instagram',
              label: 'Instagram',
              default: true
            },
            {
              type: 'checkbox',
              name: 'linkedin',
              label: 'Linkedin',
              default: true
            },
            {
              type: 'checkbox',
              name: 'twitter',
              label: 'Twitter',
              default: true
            },
            {
              type: 'checkbox',
              name: 'youtube',
              label: 'Youtube',
              default: false
            },
            {
              type: 'color',
              name: 'youtube',
              label: 'Youtube',
              default: false
            }
          ]
        }
      }
    })
    editor.on('component:selected', (some, argument, third) => {
      if (some.get('type') === 'image') {
        editor.runCommand('open-assets', {
          target: editor.getSelected()
        })
      }
      if (some && some.attributes) {
        self.populateStyleConfigs(some.attributes.attributes['data-block-id'], editor)
      }
    })
    editor.on('component:add', (some, argument, third) => {
      if (some.attributes && some.attributes.attributes) {
        if (some.attributes.attributes['data-block-id']) {
          self.populateStyleConfigs(some.attributes.attributes['data-block-id'], editor)
        }
      }
    })
    editor.on('load', (some, argument) => {
      const domComponents = editor.DomComponents
      const wrapper = domComponents.getWrapper()
      wrapper.set('style', { 'padding-left': '5%', 'padding-right': '5%', 'background-color': '#fff' })
    })
    editor.on('asset:add', (some, argument, third) => {
      if (some && some.id) {
        self.selectedImage = some.id
        editor.runCommand('close-assets', {
          target: editor.getSelected()
        })
      }
    })
  }

  public populateStyleConfigs (id: any, editor: any) {
    if (id && this.styleConfig[id]) {
      this.selectedElementStyleBuildProps = this.styleConfig[id].buildProps
      this.selectedElementStyle = this.styleConfig[id].style
      this.selectedElementStyle.forEach((e: any, i: any) => {
        const exists = editor.StyleManager.getProperty('Styles', e.property)
        if (exists === null) editor.StyleManager.addProperty('Styles', { ...e }, { at: i })
      })
    }
  }

  public changeSocials (e: any) {
    // @ts-ignore
    this.$parent.changeSocials(e)
  }

  public changeAllButtonsStyle (e: any, editor: any) {

  }
}
