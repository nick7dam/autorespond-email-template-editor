import grapesjsCustomCode from 'grapesjs-custom-code'
const swv = 'sw-visibility'
const expt = 'export-template'
const osm = 'open-sm'
const ogsm = 'open-gsm'
const otm = 'open-tm'
const ful = 'fullscreen'
const prv = 'preview'
export const EditorConfig = {
  colorPicker: {
    color: '#3777B4',
    showAlpha: false,
    showPalette: false,
    preferredFormat: 'hex',
    appendTo: 'parent',
    offset: {
      top: 26,
      left: -190
    }
  },
  avoidInlineStyle: 1,
  height: '100vh',
  width: '100%',
  container: '#gjs',
  fromElement: 1,
  storageManager: { type: 0 },
  showOffsets: 1,
  assetManager: {
    embedAsBase64: 1,
    assets: []
  },
  blockManager: {
    appendTo: '#blocks'
  },
  plugins: [
    'grapesjs-mjml', grapesjsCustomCode, 'grapesjs-rte-extensions', 'grapesjs-parser-postcss'
  ],
  pluginsOpts: {
    'grapesjs-custom-code': {
    },
    'grapesjs-mjml': {
      columnsPadding: '0 0',
      resetStyleManager: false,
      fonts: []
    },
    'grapesjs-rte-extensions': {
      list: true,
      format: {
        heading1: false,
        heading2: false,
        heading3: false,
        paragraph: false,
        clearFormatting: false
      },
      actions: true,
      darkColorPicker: true,
      undoredo: true
    }
  },
  styleManager: {
    clearProperties: 1,
    sectors: [{
      id: 'Styles',
      name: 'Styles',
      open: true,
      buildProps: [],
      properties: []
    }]
  },
  panels: {
    defaults: [
      {
        id: 'views',
        buttons: [
          {
            id: otm,
            className: 'fa fa-cog',
            command: otm,
            togglable: 1,
            attributes: { title: 'Settings' }
          },
          {
            id: osm,
            className: 'fa fa-paint-brush',
            command: osm,
            active: true,
            togglable: 1,
            attributes: { title: 'Open Style Manager' }
          },
          {
            id: ogsm,
            className: 'fa fa-globe',
            command: ogsm,
            active: true,
            togglable: 1,
            attributes: { title: 'Open Global Style' }
          }]
      },
      {
        id: 'commands',
        buttons: [{
          active: true,
          id: swv,
          className: 'fa fa-square-o',
          command: swv,
          context: swv,
          attributes: { title: 'View components' }
        },
        {
          id: prv,
          className: 'fa fa-eye',
          command: prv,
          context: prv,
          attributes: { title: 'Preview' }
        },
        {
          id: expt,
          className: 'fa fa-code',
          command: expt,
          attributes: { title: 'View code' }
        }, {
          id: ful,
          className: 'fa fa-arrows-alt',
          command: ful,
          context: ful,
          attributes: { title: 'Fullscreen' }
        }, {
          id: 'undo',
          className: 'fa fa-undo',
          command: e => e.runCommand('core:undo')
        }, {
          id: 'redo',
          className: 'fa fa-repeat',
          command: e => e.runCommand('core:redo')
        }]
      }
    ]
  }
}
